#ifndef _RAPID_PIPO_TOOLS_H_
#define _RAPID_PIPO_TOOLS_H_

#include "rapidPiPoHost.h"

typedef RapidPiPoStreamAttributes signalAttributes;
typedef RapidPiPoHost signalProcessingHost;

#endif /* _RAPID_PIPO_TOOLS_H_ */
