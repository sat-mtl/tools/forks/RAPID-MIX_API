#ifndef _RAPID_PIPO_HOST_H_
#define _RAPID_PIPO_HOST_H_

#include "PiPo.h"
#include "PiPoHost.h"

//#ifdef EXTERNAL_JSONCPP_PATH
#define EXTERNAL_JSONCPP_PATH "json.h"
#include EXTERNAL_JSONCPP_PATH
//#endif /* EXTERNAL_JSONCPP_PATH */

#define MIN_PIPO_SAMPLERATE (1.0 / 31536000000.0) /* once a year */
#define MAX_PIPO_SAMPLERATE (96000000000.0)
#define DEFAULT_PIPO_SAMPLERATE 1000.0

struct RapidPiPoStreamAttributes {
  RapidPiPoStreamAttributes() : // default parameters suited for audio
  hasTimeTags(false),
  rate(DEFAULT_PIPO_SAMPLERATE),
  offset(0),
  width(1),
  height(1),
  labels({ "" }),
  hasVarSize(false),
  domain(0),
  maxFrames(1) {}

  bool hasTimeTags;
  double rate;
  double offset;
  unsigned int width;
  unsigned int height;
  std::vector<std::string> labels;
  bool hasVarSize;
  double domain;
  unsigned int maxFrames;
};

//================================ H O S T ===================================//

class RapidPiPoHost : public PiPoHost {
public:
  virtual int setInputStreamAttributes(RapidPiPoStreamAttributes &a);
  virtual void onNewFrame(double time, double weight, PiPoValue *values, unsigned int size);

  /** Get a JSON representation of the model in the form of a styled string */
  virtual std::string getJSON();
  /** Write a JSON model description to specified file path */
  virtual void writeJSON(const std::string &filepath);
  /** configure empty model with string. See getJSON() */
  virtual bool putJSON(const std::string &jsonMessage);
  /** read a JSON file at file path and build a modelSet from it */
  virtual bool readJSON(const std::string &filepath);

private:
  Json::Value toJSON();
  bool fromJSON(Json::Value &jv);
};

#endif /* _RAPID_PIPO_HOST_H_ */
