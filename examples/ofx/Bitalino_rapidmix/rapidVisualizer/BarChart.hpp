//
//  BarChart.hpp
//  RapidVisualizerOSC
//
//  Created by James on 09/11/2017.
//
//

#ifndef BarChart_hpp
#define BarChart_hpp

#include <stdio.h>
#include "RealTimeGraph.hpp"

class BarChart : public RealTimeGraph
{
public:
    BarChart ( GraphState* state ) : RealTimeGraph (state)
    {
        //
    }
    ~BarChart ( void )
    {
        //
    }
    void updateRep ( void )
    {
        //
    }
    void drawSubGraph ( std::string subLabel, DataContainer<std::vector<double>>& data, ofRectangle subLabelRect )
    {
        double
        minIn = 0,
        minOut = 0,
        maxOut = -subLabelRect.height,
        startPosY = subLabelRect.height,
        barSize = subLabelRect.width/data.labelData.size(),
        separation = barSize/16,
        halfSeparation = separation/2;
        bool drawZeroSep = false;
        
        if (data.minValue < 0)
        { // Add negative part
            startPosY = subLabelRect.height/2;
            minIn = -data.maxValue;
            minOut = subLabelRect.height/2;
            maxOut /= 2;
            drawZeroSep = true;
        }
        for (uint32_t i = 0; i < data.labelData.size(); ++i)
        {
            double output = mapVals(data.labelData[i], minIn, data.maxValue, minOut, maxOut );
            ofSetColor (graphColor);
            ofFill();
            ofDrawRectangle(subLabelRect.x + barSize * i + halfSeparation, subLabelRect.y + startPosY, barSize - separation, output );
            
        }
        if (drawZeroSep)
        {
            ofSetLineWidth(1.25);
            ofSetColor (175,150,150);
            ofDrawLine(subLabelRect.x, subLabelRect.y + startPosY,
                       subLabelRect.x + subLabelRect.width, subLabelRect.y + startPosY);
        }
    }
    void update ( void )
    {
        //
    }
};

#endif /* BarChart_hpp */
