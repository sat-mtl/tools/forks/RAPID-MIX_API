//
//  HistoryGraph.hpp
//  RapidVisualizerOSC
//
//  Created by James on 09/11/2017.
//
//

#ifndef HistoryGraph_h
#define HistoryGraph_h

#include "Graph.hpp"
#include <list>

class HistoryGraph : public Graph
{
public:
    HistoryGraph ( GraphState* state )
    : Graph(state)
    {
        
    }
    ~HistoryGraph ( void )
    {
    }
    
    virtual void updateRep ( void ) = 0; // update representation
    virtual void drawSubGraph ( std::string subLabel, DataContainer<std::list<double>>& data, ofRectangle subLabelrect ) = 0;
    virtual void update ( void ) = 0;
    
    void addData ( std::string subLabel, std::vector<double>& data )
    {
        if (data.size() < state->historySize)
        {
            //FIXME: can be sped up by using the result of this search instead of accessing by[] again
            if ( subLabelData.find(subLabel) == subLabelData.end() ) {
                // not found
                DataContainer<std::list<double>> container;
                container.labelData.resize(state->historySize);
                std::fill(container.labelData.begin(), container.labelData.end(), 0.0);
                subLabelData[subLabel] = container;
                
            }
            DataContainer<std::list<double>>&  dataRef = subLabelData[subLabel];
            list<double>& referencedList = dataRef.labelData;
            while (referencedList.size() + data.size() >= state->historySize)
            {
                referencedList.pop_front();
            }
            for (int32_t i = data.size()-1; i >= 0; --i)
            {
                double val = data[i];
                if (val < dataRef.minValue)
                    dataRef.minValue = val;
                if (val > dataRef.maxValue)
                    dataRef.maxValue = val;
                if (fabs(val) > dataRef.maxValue)
                    dataRef.maxValue = fabs(val);
                referencedList.push_back(val);
            }
        }
    }
    
    void reset ( void )
    {
        subLabelData.clear();
    }
    
    void draw ( void )
    {
        uint32_t numElements = subLabelData.size();
        uint16_t heightBetweenSubLabels = state->positionAndSize.height/numElements;
        uint16_t subLabelY = 0;
        
        ofSetColor (textColor);
        ofDrawLine(state->positionAndSize.x, state->positionAndSize.y + heightBetweenSubLabels - 1.5,
                   state->positionAndSize.x + state->positionAndSize.width, state->positionAndSize.y + heightBetweenSubLabels - 1.5);
         
        std::map<std::string, DataContainer<std::list<double>>>::iterator it;
        for(it = subLabelData.begin(); it != subLabelData.end(); ++it)
        {
            std::string subLabel = it->first;
            DataContainer<std::list<double>>& data = it->second;
            
            drawSubGraph (subLabel, data, ofRectangle(state->positionAndSize.x,
                                                      state->positionAndSize.y + subLabelY + 3,
                                                      state->positionAndSize.width,
                                                      heightBetweenSubLabels - 6));
            // Draw label and background
            drawTextLabel(subLabel, ofVec2f(state->positionAndSize.x + state->positionAndSize.width,
                                            state->positionAndSize.y + subLabelY),
                          ofColor(100, 100, 100), ofColor(textColor), TextAlignment::RIGHT, false);
            
            
            // Draw max value
            drawTextLabel(ofToString(data.maxValue),
                          ofVec2f(state->positionAndSize.x + state->positionAndSize.width/2,
                                  state->positionAndSize.y + subLabelY),
                          ofColor(50, 50, 50), ofColor(255, 255, 255), TextAlignment::CENTER, false);
            
            // Increment Y position
            subLabelY += heightBetweenSubLabels;
            
            // Draw min value
            // Show actual min value?
            drawTextLabel(ofToString((data.minValue < 0) ? -data.maxValue : 0),
                          ofVec2f(state->positionAndSize.x + state->positionAndSize.width/2,
                                  state->positionAndSize.y + subLabelY),
                          ofColor(50, 50, 50), ofColor(255, 255, 255), TextAlignment::CENTER, true);
            
            // Draw Line at bottom of graph
            //ofSetLineWidth(2.0);
            //ofSetColor (180,180,180);
            /*ofDrawLine(state->positionAndSize.x, state->positionAndSize.y + subLabelY + 3,
                       state->positionAndSize.x + state->positionAndSize.width, state->positionAndSize.y + subLabelY + 3);*/
        }
    }
    uint32_t getNumSubGraphs ( void ) const
    {
        return subLabelData.size();
    }
protected:
    std::map <std::string, DataContainer<std::list<double>>> subLabelData;
};
#endif /* HistoryGraph_h */
