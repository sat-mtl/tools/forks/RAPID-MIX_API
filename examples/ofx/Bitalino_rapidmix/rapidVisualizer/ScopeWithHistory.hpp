//
//  ScopeWithHistory.hpp
//  RapidVisualizerOSC
//
//  Created by James on 12/11/2017.
//
//

#ifndef ScopeWithHistory_h
#define ScopeWithHistory_h

#include <stdio.h>
#include "Graph.hpp"

class ScopeWithHistory : public Graph
{
public:
    ScopeWithHistory ( GraphState* state ) : Graph (state)
    {
        //
    }
    ~ScopeWithHistory ( void )
    {
        //
    }
    void addData ( std::string subLabel, std::vector<double>& data )
    {
        if (data.size() < state->historySize)
        {
            //FIXME: can be sped up by using the result of this search instead of accessing by[] again
            if ( subLabelData.find(subLabel) == subLabelData.end() ) {
                // not found
                DataContainer<std::vector<double>> container;
                container.labelData.resize(state->historySize);
                std::fill(container.labelData.begin(), container.labelData.end(), 0.0);
                subLabelData[subLabel] = container;
            }
            DataContainer<std::vector<double>>&  dataRef = subLabelData[subLabel];
            std::vector<double>& referencedList = dataRef.labelData;
            while (referencedList.size() >= state->historySize)
            {
                referencedList.pop_back();
            }
            for (int32_t i = data.size()-1; i >= 0; --i)
            {
                double val = data[i];
                if (val < dataRef.minValue)
                    dataRef.minValue = val;
                if (val > dataRef.maxValue)
                    dataRef.maxValue = val;
                if (fabs(val) > dataRef.maxValue)
                    dataRef.maxValue = fabs(val);
                if (referencedList.size() + data.size() >= state->historySize)
                {
                    referencedList[dataRef.iteratorPos] = val;
                    dataRef.iteratorPos = (dataRef.iteratorPos + data.size()) % state->historySize;
                } else {
                    referencedList.insert(referencedList.begin(), val);
                }
            }
        }
    }
    void updateRep ( void )
    {
        //
    }
    void drawSubGraph ( std::string subLabel, DataContainer<std::vector<double>>& data, ofRectangle subLabelRect )
    {
        double
        minIn = 0,
        minOut = 0,
        maxOut = -subLabelRect.height,
        startPosY = subLabelRect.height,
        pointDistance = subLabelRect.width/data.labelData.size(),
        separation = pointDistance/2;
        //halfSeparation = separation/2;
        bool drawZeroSep = false;
        
        if (data.minValue < 0)
        { // Add negative part
            startPosY = subLabelRect.height/2;
            minIn = -data.maxValue;
            minOut = subLabelRect.height/2;
            maxOut /= 2;
            drawZeroSep = true;
        }
        
        ofBeginShape();
        ofFill();
        ofVertex(subLabelRect.x, subLabelRect.y + startPosY);
        double output = mapVals(data.labelData.front(), minIn, data.maxValue, minOut, maxOut );
        ofVertex(subLabelRect.x, subLabelRect.y + startPosY + output);
        uint32_t i = 0;
        for (double d : data.labelData)
        {
            output = mapVals(d, minIn, data.maxValue, minOut, maxOut );
            ofSetColor (graphColor);
            
            ofVertex(subLabelRect.x + pointDistance * i + separation, subLabelRect.y + startPosY + output);
            //ofDrawRectangle(subLabelRect.x + barSize * i + halfSeparation, subLabelRect.y + startPosY, barSize - separation, output );
            ++i;
            
        }
        output = mapVals(data.labelData.back(), minIn, data.maxValue, minOut, maxOut );
        ofVertex(subLabelRect.x + subLabelRect.width, subLabelRect.y + startPosY + output);
        ofVertex(subLabelRect.x + subLabelRect.width, subLabelRect.y + startPosY);
        ofEndShape();
        
        if (drawZeroSep)
        {
            ofSetLineWidth(1.25);
            ofSetColor (175,150,150);
            ofDrawLine(subLabelRect.x, subLabelRect.y + startPosY,
                       subLabelRect.x + subLabelRect.width, subLabelRect.y + startPosY);
        }
    }
    void update ( void )
    {
        //
    }
    
    void reset ( void )
    {
        subLabelData.clear();
    }
    
    void draw ( void )
    {
        uint32_t numElements = subLabelData.size();
        uint16_t heightBetweenSubLabels = state->positionAndSize.height/numElements;
        uint16_t subLabelY = 0;
        
        ofSetColor (255,255,255);
        ofDrawLine(state->positionAndSize.x, state->positionAndSize.y,
                   state->positionAndSize.x + state->positionAndSize.width, state->positionAndSize.y);
        
        std::map<std::string, DataContainer<std::vector<double>>>::iterator it;
        for(it = subLabelData.begin(); it != subLabelData.end(); ++it)
        {
            std::string subLabel = it->first;
            DataContainer<std::vector<double>>& data = it->second;
            
            drawSubGraph (subLabel, data, ofRectangle(state->positionAndSize.x,
                                                      state->positionAndSize.y + subLabelY,
                                                      state->positionAndSize.width,
                                                      heightBetweenSubLabels));
            // Draw label and background
            drawTextLabel(subLabel, ofVec2f(state->positionAndSize.x + state->positionAndSize.width,
                                            state->positionAndSize.y + subLabelY),
                          ofColor(100, 100, 100), ofColor(textColor), TextAlignment::RIGHT, false);
            
            
            // Draw max value
            drawTextLabel(ofToString(data.maxValue),
                          ofVec2f(state->positionAndSize.x + state->positionAndSize.width/2,
                                  state->positionAndSize.y + subLabelY),
                          ofColor(50, 50, 50), ofColor(255, 255, 255), TextAlignment::CENTER, false);
            
            // Increment Y position
            subLabelY += heightBetweenSubLabels;
            
            // Draw min value
            // Show actual min value?
            drawTextLabel(ofToString((data.minValue < 0) ? -data.maxValue : 0),
                          ofVec2f(state->positionAndSize.x + state->positionAndSize.width/2,
                                  state->positionAndSize.y + subLabelY),
                          ofColor(50, 50, 50), ofColor(255, 255, 255), TextAlignment::CENTER, true);
            
            // Draw Line at bottom of graph
            ofSetLineWidth(2.0);
            ofSetColor (180,180,180);
            ofDrawLine(state->positionAndSize.x, state->positionAndSize.y + subLabelY,
                       state->positionAndSize.x + state->positionAndSize.width, state->positionAndSize.y + subLabelY);
        }
    }
    uint32_t getNumSubGraphs ( void ) const
    {
        return subLabelData.size();
    }
protected:
    std::map <std::string, DataContainer<std::vector<double>>> subLabelData;
};

#endif /* ScopeWithHistory_h */
