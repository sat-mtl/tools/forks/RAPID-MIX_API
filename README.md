**RAPID-MIX API** is an easy-to-use toolkit designed to make sensor integration, machine learning and interactive audio accessible for artists, designers, makers, educators, and beginners, as well as creative companies and independent developers.

It has been built with RAPID-MIX technologies, that make it easy to combine sensor data, machine learning algorithms and interactive audio. They provide a full set of functionalities for cross-device and cross-platform development, modular components, and cloud-based services and multimodal data storage.

## Dependencies
Use `git submodule update --init --recursive` to pull the following library dependencies.  
1. RapidLib
1. XMM
1. PiPo
1. GVF
1. RepoVizz2 Client

## Documentation
Full documentation at http://www.rapidmixapi.com/


## Testing   
We are using Catch for C++ testing. Look at the test fixtures in the /tests/test_projetc/test_project.xcodeproj for an example of how to implement them.

## Building with CMake
Navigate to /RAPID-MIX_API and run this in a terminal:

```bash
mkdir build  
cd build  
cmake ..  
make  
```

Or run the shell script:

```bash
./rmix_build_test.sh
```

On Linux you will have to compile the project with Clang, as some numeric complex literals are not defined with GCC:

```bash
mkdir build
cd build
CC=clang CXX=clang++ cmake ..
make
```

## Python wrapper
If Python environment files are found, a Python wrapper is built alongside the RAPID-MIX_API library. To install it, just (after having built the library):

```bash
sudo make install
```

Then try loading the resulting Python module:
```python
import pyrapidmix
```
