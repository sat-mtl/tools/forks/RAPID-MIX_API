#!/bin/bash

#Make sure we've got the latest version
git pull
git submodule update --init --recursive

rm -rf build/
mkdir build
cd build
cmake ..
make
./rapidmixTest
cp ./helloRapidMix ../examples/HelloRapidMix/helloRapidMix