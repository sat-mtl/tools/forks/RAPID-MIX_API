//
//  RapidVisualization.hpp
//  RapidVisualizerOSC
//
//  Created by James on 10/11/2017.
//
//

#ifndef RapidVisualization_h
#define RapidVisualization_h

#include "RapidVisualizer.hpp"

class RapidVisualization
{
public:
    RapidVisualization ( void );
    ~RapidVisualization ( void );
    
    void setup ( ofRectangle posAndSize, uint32_t defaultHistorySize=256 );
    
    // Append multiple types of graphs to cycle through as new entry addresses are added
    // Implement?
    //RapidVisualizer* addGraphType ( RapidVisualizer::graphTypeRealtime graphType );
    //RapidVisualizer* addGraphType ( RapidVisualizer::graphTypeWithHistory graphType, uint32_t historySize );
    
    void addData ( std::string& address, std::vector<double>& data );
    void addData ( std::string& graphName, std::string& subGraphName, std::vector<double>& data );
    void reset ( void );
    
    void update ( void );
    void draw ( void );
    
    void setPos ( ofVec2f pos );
    void setSize ( ofVec2f size );
    void setPosAndSize ( ofRectangle posAndSize );
    
    void setGuiHidden ( bool guiHidden );
    
private:
    bool guiHidden = false;
    uint32_t numGraphs = 0;
    std::map<std::string, RapidVisualizer*> visualizers;
    ofRectangle posAndSize;
    
    uint32_t defaultHistorySize;
    
    std::pair<std::string, std::string> getRoute ( std::string& address );
    void updateRep ( void );
};

#endif /* RapidVisualization_h */
