//
//  RapidVisualization.cpp
//  RapidVisualizerOSC
//
//  Created by James on 13/11/2017.
//
//

#include <stdio.h>
#include "RapidVisualization.hpp"

RapidVisualization::RapidVisualization ( void )
{
    
}

RapidVisualization::~RapidVisualization ( void )
{
    std::map<std::string, RapidVisualizer*>::iterator it;
    for(it = visualizers.begin(); it != visualizers.end(); ++it)
    {
        if (it->second)
            delete it->second;
    }
}

void RapidVisualization::setup ( ofRectangle posAndSize, uint32_t defaultHistorySize )
{
    this->posAndSize = posAndSize;
    this->defaultHistorySize = defaultHistorySize;
}

std::pair<std::string, std::string> RapidVisualization::getRoute(std::string& address)
{
    std::pair<std::string, std::string> ret;
    size_t delimPos = address.substr(1).find("/");
    if (delimPos != std::string::npos)
    {
        delimPos += 1;
        ret.first = address.substr(0, delimPos);
        ret.second = address.substr(delimPos);
    }
    else
    {
        ret.first = address.substr(0, address.length());
        ret.second = "/";
    }
    return ret;
}

void RapidVisualization::addData ( std::string& address, std::vector<double>& data )
{
    std::pair<std::string, std::string> route = getRoute(address);
    if ( visualizers.find(route.first) == visualizers.end() ) {
        RapidVisualizer* ptr = visualizers[route.first] = new RapidVisualizer(); // Add new graph
        ptr->setup(route.first, RapidVisualizer::LINE_GRAPH_WITH_HISTORY, defaultHistorySize, Graph::GRAPHS_STACKED, false, ofRectangle(0,0,100,100));
        updateRep();
    }
    
    RapidVisualizer* rapidVizPtr = visualizers[route.first];
    rapidVizPtr->addData(address, data);
}

void RapidVisualization::addData ( std::string& graphName, std::string& subGraphName, std::vector<double>& data )
{
    if ( visualizers.find(graphName) == visualizers.end() ) {
        RapidVisualizer* ptr = visualizers[graphName] = new RapidVisualizer(); // Add new graph
        ptr->setup(graphName, RapidVisualizer::LINE_GRAPH_WITH_HISTORY, defaultHistorySize, Graph::GRAPHS_STACKED, false, ofRectangle(0,0,100,100));
        updateRep();
    }
    
    RapidVisualizer* rapidVizPtr = visualizers[graphName];
    rapidVizPtr->addData(subGraphName, data);
}

void RapidVisualization::reset ( void )
{
    std::map<std::string, RapidVisualizer*>::iterator it;
    for(it = visualizers.begin(); it != visualizers.end(); ++it)
    {
        if (it->second)
            delete it->second;
    }
    visualizers.clear();
}

void RapidVisualization::update ( void )
{
    uint32_t tempNumGraphs = 0;
    bool resetCase = false; // This is terrible  (stops jitter when selecting new graph type)
    std::map<std::string, RapidVisualizer*>::iterator it;
    for(it = visualizers.begin(); it != visualizers.end(); ++it)
    {
        it->second->update();
        uint32_t tGraph = it->second->getNumGraphs();
        if (tGraph == 0)
        {
            resetCase = true;
            break;
        }
        tempNumGraphs += tGraph;
    }
    if (tempNumGraphs != numGraphs && !resetCase)
    {
        numGraphs = tempNumGraphs;
        updateRep();
    }
}

void RapidVisualization::draw ( void )
{
    std::map<std::string, RapidVisualizer*>::iterator it;
    for(it = visualizers.begin(); it != visualizers.end(); ++it)
    {
        it->second->draw();
    }
    if (!guiHidden)
    for(it = visualizers.begin(); it != visualizers.end(); ++it)
    {
        it->second->drawMenu(posAndSize); // Stop menu being behind next graph
    }
    if (numGraphs == 0)
    {
        drawTextLabel("Waiting for OSC messages on port 8338", ofVec2f(posAndSize.x+posAndSize.width/2, posAndSize.y), ofColor(24, 219, 92), ofColor(255, 157, 117), TextAlignment::CENTER, false );
    }
}

void RapidVisualization::setPos ( ofVec2f pos )
{
    posAndSize.x = pos.x;
    posAndSize.y = pos.y;
    updateRep();
}
void RapidVisualization::setSize ( ofVec2f size )
{
    posAndSize.width = size.x;
    posAndSize.height = size.y;
    updateRep();
}

void RapidVisualization::setPosAndSize ( ofRectangle posAndSize )
{
    this->posAndSize = posAndSize;
    updateRep();
}

void RapidVisualization::updateRep ( void )
{
    double graphHeight = posAndSize.height/numGraphs; // Height of single graph
    std::map<std::string, RapidVisualizer*>::iterator it;
    double drawHeight = posAndSize.y;
    double height;
    for(it = visualizers.begin(); it != visualizers.end(); ++it)
    {
        height = it->second->getNumGraphs()*graphHeight;
        it->second->setRect(ofRectangle(posAndSize.x,
                                       drawHeight,
                                       posAndSize.width,
                                       height));
        
        drawHeight += height;
    }
}

void RapidVisualization::setGuiHidden ( bool guiHidden )
{
    this->guiHidden = guiHidden;
}
