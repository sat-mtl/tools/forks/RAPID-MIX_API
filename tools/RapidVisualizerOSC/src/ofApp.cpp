#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    receiver.setup ( PORT );
    rv.setup(ofRectangle(0,0,1024,768));
    current_msg_string = 0;
    ofBackground(30, 30, 30);
}


//--------------------------------------------------------------
void ofApp::update(){
    static float a = 0;
    while( receiver.hasWaitingMessages() )
    {
        
        ofxOscMessage m;
        receiver.getNextMessage(m);
        std::string address = m.getAddress();
        std::vector<double> values;
        for ( int i=0; i<m.getNumArgs(); i++ )
        {
            double value = 0.0;
            // display the argument - make sure we get the right type
            if( m.getArgType( i ) == OFXOSC_TYPE_INT32 )
                value = static_cast<double>( m.getArgAsInt32( i ) );
            else if( m.getArgType( i ) == OFXOSC_TYPE_INT64 )
                value = static_cast<double>( m.getArgAsInt64( i ) );
            else if( m.getArgType( i ) == OFXOSC_TYPE_FLOAT )
                value = static_cast<double>( m.getArgAsFloat( i ) );
            else if( m.getArgType( i ) == OFXOSC_TYPE_DOUBLE)
                value = static_cast<double>( m.getArgAsDouble( i ) );
            else
                printf("Unknown datatype\n");
            
            values.push_back(value);
         
        }
        
        rv.addData(address, values);
    }
    rv.update();
}

//--------------------------------------------------------------
void ofApp::draw(){
    ofBackgroundGradient(ofColor(0,0,0), ofColor(10,10,10));
    rv.draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    if(key == 'h'){
        guiHide = !guiHide;
        rv.setGuiHidden(guiHide);
    }
    if(key == 'c'){
        rv.reset();
    }
    if(key == 'm'){
        // Show main menu
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
    rv.setSize(ofVec2f(w, h));
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
