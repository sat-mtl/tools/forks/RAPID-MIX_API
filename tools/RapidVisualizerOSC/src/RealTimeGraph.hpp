//
//  RealTimeGraph.hpp
//  RapidVisualizerOSC
//
//  Created by James on 09/11/2017.
//
//

#ifndef RealTimeGraph_h
#define RealTimeGraph_h

#include <map>
#include <vector>
#include <string>
#include "Graph.hpp"

class RealTimeGraph : public Graph
{
public:
    RealTimeGraph ( GraphState* state )
    : Graph(state)
    {
    }
    ~RealTimeGraph ( void )
    {
    }
    
    void addData ( std::string subLabel, std::vector<double>& data )
    {
        if ( subLabelData.find(subLabel) == subLabelData.end() ) {
            // not found
            subLabelData[subLabel] = DataContainer<std::vector<double>>();
        }
        
        subLabelData[subLabel].labelData = data;
        subLabelData[subLabel].updateMinMax();
    }
    
    virtual void reset ( void )
    {
        subLabelData.clear();
    }
    
    virtual void updateRep ( void ) = 0; // update representation
    virtual void drawSubGraph ( std::string subLabel, DataContainer<std::vector<double>>& data, ofRectangle subLabelrect ) = 0;
    
    virtual void update ( void ) = 0;
    
    void draw ( void )
    {
        uint32_t numElements = subLabelData.size();
        uint16_t heightBetweenSubLabels = state->positionAndSize.height/numElements;
        uint16_t subLabelY = 0;
        
        ofSetColor (255,255,255);
        ofDrawLine(state->positionAndSize.x, state->positionAndSize.y,
                   state->positionAndSize.x + state->positionAndSize.width, state->positionAndSize.y);
        
        std::map<std::string, DataContainer<std::vector<double>>>::iterator it;
        for(it = subLabelData.begin(); it != subLabelData.end(); ++it)
        {
            std::string subLabel = it->first;
            DataContainer<std::vector<double>>& data = it->second;
            
            drawSubGraph (subLabel, data, ofRectangle(state->positionAndSize.x,
                                                           state->positionAndSize.y + subLabelY,
                                                           state->positionAndSize.width,
                                                           heightBetweenSubLabels));
            // Draw label and background
            drawTextLabel(subLabel, ofVec2f(state->positionAndSize.x + state->positionAndSize.width,
                                            state->positionAndSize.y + subLabelY),
                          ofColor(100, 100, 100), ofColor(textColor), TextAlignment::RIGHT, false);
            
            
            // Draw max value
            drawTextLabel(ofToString(data.maxValue),
                          ofVec2f(state->positionAndSize.x + state->positionAndSize.width/2,
                                            state->positionAndSize.y + subLabelY),
                          ofColor(50, 50, 50), ofColor(255, 255, 255), TextAlignment::CENTER, false);

            // Increment Y position
            subLabelY += heightBetweenSubLabels;
            
            // Draw min value
            // Could show actually found min value
            drawTextLabel(ofToString((data.minValue < 0) ? -data.maxValue : 0),
                          ofVec2f(state->positionAndSize.x + state->positionAndSize.width/2,
                                  state->positionAndSize.y + subLabelY),
                          ofColor(50, 50, 50), ofColor(255, 255, 255), TextAlignment::CENTER, true);
            
            // Draw Line at bottom of graph
            ofSetLineWidth(2.0);
            ofSetColor (180,180,180);
            ofDrawLine(state->positionAndSize.x, state->positionAndSize.y + subLabelY,
                       state->positionAndSize.x + state->positionAndSize.width, state->positionAndSize.y + subLabelY);
        }
    }
    
    uint32_t getNumSubGraphs ( void ) const
    {
        return subLabelData.size();
    }
protected:
    std::map <std::string, DataContainer<std::vector<double>>> subLabelData;
};

#endif /* RealTimeGraph_h */
