#ifndef CATCH_CONFIG_MAIN
#define CATCH_CONFIG_MAIN
#endif

#include "catch.hpp"
#include "machineLearning.h"

//============================= TRAINING DATA ================================//

SCENARIO("Test training sets managements", "[machineLearning]")
{
    GIVEN("A training set we fill in a variety of ways")
    {
        rapidmix::trainingData set;

        set.createNewPhrase("test_label_01");

        std::vector <std::pair <std::vector<double>, std::vector<double>>> phrase = {
            { { 0.0, 0.0 }, { 9.0 } },
            { { 1.0, 0.0 }, { 8.0 } },
            { { 2.0, 0.0 }, { 7.0 } },
            { { 3.0, 0.0 }, { 6.0 } },
            { { 4.0, 0.0 }, { 5.0 } },
            { { 5.0, 0.0 }, { 4.0 } },
            { { 6.0, 0.0 }, { 3.0 } },
            { { 7.0, 0.0 }, { 2.0 } },
            { { 8.0, 0.0 }, { 1.0 } },
            { { 9.0, 0.0 }, { 0.0 } }
        };

        for (int i = 0; i < phrase.size(); ++i) {
            set.addElement(phrase[i].first, phrase[i].second);
        }

        WHEN("We serialize / deserialize a training set")
        {
            std::string js = set.getJSON();
            std::cout << js << std::endl;

            THEN("It should stay the same")
            {

            }
        }
    }
}
