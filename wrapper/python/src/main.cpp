#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "./rapidmix.h"

namespace rm = rapidmix;
namespace py = pybind11;

PYBIND11_MODULE(pyrapidmix, m)
{
    m.doc() = R"pbdoc(
        Python wrapper for RAPID-MIX_API
        --------------------------------

        .. currentmodule:: pyrapidmix

        .. autosummary::
           :toctree: _generate

        TrainingData
        StaticClassification
        StaticRegression
        XmmStaticRegression
        XmmTemporalRegression
    )pbdoc";

    py::class_<rm::trainingData>(m, "TrainingData")
        .def(py::init<>())
        .def("start_recording", py::overload_cast<const std::string&>(&rm::trainingData::startRecording))
        .def("stop_recording", &rm::trainingData::stopRecording)
        .def("record_single_element", py::overload_cast<const std::vector<double>&, const std::vector<double>&>(&rm::trainingData::recordSingleElement))
        .def("add_element", py::overload_cast<const std::vector<double>&, const std::vector<double>&>(&rm::trainingData::addElement));

    py::class_<rm::staticClassification>(m, "StaticClassification")
        .def(py::init<>())
        .def("train", &rm::staticClassification::train)
        .def("run", py::overload_cast<const std::vector<double>&, const std::string&>(&rm::staticClassification::run))
        .def("reset", &rm::staticClassification::reset);

    py::class_<rm::staticRegression>(m, "StaticRegression")
        .def(py::init<>())
        .def("train", &rm::staticRegression::train)
        .def("run", py::overload_cast<const std::vector<double>&>(&rm::staticRegression::run))
        .def("reset", &rm::staticRegression::reset);

    py::class_<rm::xmmStaticRegression>(m, "XmmStaticRegression")
        .def(py::init())
        .def("train", &rm::xmmStaticRegression::train)
        .def("run", py::overload_cast<const std::vector<double>&>(&rm::xmmStaticRegression::run))
        .def("reset", &rm::xmmStaticRegression::reset);

    py::class_<rm::xmmTemporalRegression>(m, "XmmTemporalRegression")
        .def(py::init())
        .def("train", &rm::xmmTemporalRegression::train)
        .def("run", py::overload_cast<const std::vector<double>&>(&rm::xmmTemporalRegression::run))
        .def("reset", &rm::xmmTemporalRegression::reset);
}
