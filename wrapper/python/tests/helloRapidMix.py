#!/usr/bin/env python3
#
# This test is a translation of the HelloRapidMix example

import pyrapidmix as rm

mt_of_regression = rm.StaticRegression()
data = rm.TrainingData()

data.record_single_element([48], [130.81])
data.record_single_element([54], [185])
data.record_single_element([60], [261.63])
data.record_single_element([66], [369.994])
data.record_single_element([72], [523.25])

mt_of_regression.train(data)
assert(abs(mt_of_regression.run([30])[0] - 115.282) < 0.001)
assert(abs(mt_of_regression.run([60])[0] - 255.692) < 0.001)
assert(abs(mt_of_regression.run([90])[0] - 715.172) < 0.001)
assert(abs(mt_of_regression.run([120])[0] - 735.139) < 0.001)
